This module provide possibility to change default way of choosing layout, 
for entities, using "select" field on radio buttons with thumbnails of layouts. 
Same as provide module Panels for choosing layout.

Installation
Installation is like with all normal drupal modules: 
extract the 'panelizer_layout_thumb' folder from the tar ball to the modules directory 
from your website (typically sites/all/modules) and enable it in admin panel.